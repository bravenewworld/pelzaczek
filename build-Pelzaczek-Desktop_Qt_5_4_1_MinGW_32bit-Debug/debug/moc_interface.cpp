/****************************************************************************
** Meta object code from reading C++ file 'interface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Pelzaczek/interface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Interface_t {
    QByteArrayData data[22];
    char stringdata[369];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Interface_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Interface_t qt_meta_stringdata_Interface = {
    {
QT_MOC_LITERAL(0, 0, 9), // "Interface"
QT_MOC_LITERAL(1, 10, 14), // "Stop_algorithm"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 4), // "iter"
QT_MOC_LITERAL(4, 31, 15), // "Start_algorithm"
QT_MOC_LITERAL(5, 47, 25), // "on_StepSlider_sliderMoved"
QT_MOC_LITERAL(6, 73, 8), // "position"
QT_MOC_LITERAL(7, 82, 23), // "on_LoadFunction_clicked"
QT_MOC_LITERAL(8, 106, 16), // "on_Start_clicked"
QT_MOC_LITERAL(9, 123, 16), // "on_Pauza_clicked"
QT_MOC_LITERAL(10, 140, 15), // "on_Stop_clicked"
QT_MOC_LITERAL(11, 156, 24), // "on_actionZakoncz_clicked"
QT_MOC_LITERAL(12, 181, 22), // "on_EditAlfa_textEdited"
QT_MOC_LITERAL(13, 204, 4), // "arg1"
QT_MOC_LITERAL(14, 209, 22), // "on_EditBeta_textEdited"
QT_MOC_LITERAL(15, 232, 23), // "on_EditGamma_textEdited"
QT_MOC_LITERAL(16, 256, 25), // "on_EditEpsilon_textEdited"
QT_MOC_LITERAL(17, 282, 31), // "on_IterationAmount_valueChanged"
QT_MOC_LITERAL(18, 314, 13), // "on_reflection"
QT_MOC_LITERAL(19, 328, 12), // "on_expansion"
QT_MOC_LITERAL(20, 341, 14), // "on_contraction"
QT_MOC_LITERAL(21, 356, 12) // "on_reduction"

    },
    "Interface\0Stop_algorithm\0\0iter\0"
    "Start_algorithm\0on_StepSlider_sliderMoved\0"
    "position\0on_LoadFunction_clicked\0"
    "on_Start_clicked\0on_Pauza_clicked\0"
    "on_Stop_clicked\0on_actionZakoncz_clicked\0"
    "on_EditAlfa_textEdited\0arg1\0"
    "on_EditBeta_textEdited\0on_EditGamma_textEdited\0"
    "on_EditEpsilon_textEdited\0"
    "on_IterationAmount_valueChanged\0"
    "on_reflection\0on_expansion\0on_contraction\0"
    "on_reduction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Interface[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x06 /* Public */,
       4,    1,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,  105,    2, 0x08 /* Private */,
       7,    0,  108,    2, 0x08 /* Private */,
       8,    0,  109,    2, 0x08 /* Private */,
       9,    0,  110,    2, 0x08 /* Private */,
      10,    0,  111,    2, 0x08 /* Private */,
      11,    0,  112,    2, 0x08 /* Private */,
      12,    1,  113,    2, 0x08 /* Private */,
      14,    1,  116,    2, 0x08 /* Private */,
      15,    1,  119,    2, 0x08 /* Private */,
      16,    1,  122,    2, 0x08 /* Private */,
      17,    1,  125,    2, 0x08 /* Private */,
      18,    0,  128,    2, 0x08 /* Private */,
      19,    0,  129,    2, 0x08 /* Private */,
      20,    0,  130,    2, 0x08 /* Private */,
      21,    0,  131,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Interface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Interface *_t = static_cast<Interface *>(_o);
        switch (_id) {
        case 0: _t->Stop_algorithm((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->Start_algorithm((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_StepSlider_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_LoadFunction_clicked(); break;
        case 4: _t->on_Start_clicked(); break;
        case 5: _t->on_Pauza_clicked(); break;
        case 6: _t->on_Stop_clicked(); break;
        case 7: _t->on_actionZakoncz_clicked(); break;
        case 8: _t->on_EditAlfa_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->on_EditBeta_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->on_EditGamma_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->on_EditEpsilon_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->on_IterationAmount_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_reflection(); break;
        case 14: _t->on_expansion(); break;
        case 15: _t->on_contraction(); break;
        case 16: _t->on_reduction(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Interface::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Interface::Stop_algorithm)) {
                *result = 0;
            }
        }
        {
            typedef void (Interface::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Interface::Start_algorithm)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject Interface::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Interface.data,
      qt_meta_data_Interface,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Interface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Interface::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Interface.stringdata))
        return static_cast<void*>(const_cast< Interface*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Interface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void Interface::Stop_algorithm(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Interface::Start_algorithm(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
