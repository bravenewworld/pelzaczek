/********************************************************************************
** Form generated from reading UI file 'interface.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_Interface
{
public:
    QAction *actionZaladuj_funkcje;
    QAction *actionZakoncz;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QCustomPlot *wykres;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *LoadFunction;
    QPushButton *Start;
    QPushButton *Pauza;
    QPushButton *Stop;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QSlider *StepSlider;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *EditBeta;
    QLineEdit *EditAlfa;
    QLabel *label_5;
    QSpinBox *IterationAmount;
    QLabel *label_4;
    QLineEdit *EditGamma;
    QLabel *label_2;
    QLineEdit *EditEpsilon;
    QLabel *label_3;
    QMenuBar *menuBar;
    QMenu *menuPelzaczek;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Interface)
    {
        if (Interface->objectName().isEmpty())
            Interface->setObjectName(QStringLiteral("Interface"));
        Interface->resize(695, 603);
        actionZaladuj_funkcje = new QAction(Interface);
        actionZaladuj_funkcje->setObjectName(QStringLiteral("actionZaladuj_funkcje"));
        actionZakoncz = new QAction(Interface);
        actionZakoncz->setObjectName(QStringLiteral("actionZakoncz"));
        centralWidget = new QWidget(Interface);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(9, 9, 511, 481));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        wykres = new QCustomPlot(horizontalLayoutWidget);
        wykres->setObjectName(QStringLiteral("wykres"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(wykres->sizePolicy().hasHeightForWidth());
        wykres->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(wykres);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(540, 300, 121, 191));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        LoadFunction = new QPushButton(verticalLayoutWidget);
        LoadFunction->setObjectName(QStringLiteral("LoadFunction"));

        verticalLayout->addWidget(LoadFunction);

        Start = new QPushButton(verticalLayoutWidget);
        Start->setObjectName(QStringLiteral("Start"));

        verticalLayout->addWidget(Start);

        Pauza = new QPushButton(verticalLayoutWidget);
        Pauza->setObjectName(QStringLiteral("Pauza"));

        verticalLayout->addWidget(Pauza);

        Stop = new QPushButton(verticalLayoutWidget);
        Stop->setObjectName(QStringLiteral("Stop"));

        verticalLayout->addWidget(Stop);

        verticalLayoutWidget_3 = new QWidget(centralWidget);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(19, 510, 501, 31));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        StepSlider = new QSlider(verticalLayoutWidget_3);
        StepSlider->setObjectName(QStringLiteral("StepSlider"));
        StepSlider->setOrientation(Qt::Horizontal);

        verticalLayout_3->addWidget(StepSlider);

        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(540, 10, 135, 156));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        EditBeta = new QLineEdit(layoutWidget);
        EditBeta->setObjectName(QStringLiteral("EditBeta"));

        formLayout->setWidget(3, QFormLayout::FieldRole, EditBeta);

        EditAlfa = new QLineEdit(layoutWidget);
        EditAlfa->setObjectName(QStringLiteral("EditAlfa"));

        formLayout->setWidget(2, QFormLayout::FieldRole, EditAlfa);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        IterationAmount = new QSpinBox(layoutWidget);
        IterationAmount->setObjectName(QStringLiteral("IterationAmount"));

        formLayout->setWidget(1, QFormLayout::FieldRole, IterationAmount);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_4);

        EditGamma = new QLineEdit(layoutWidget);
        EditGamma->setObjectName(QStringLiteral("EditGamma"));

        formLayout->setWidget(4, QFormLayout::FieldRole, EditGamma);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        EditEpsilon = new QLineEdit(layoutWidget);
        EditEpsilon->setObjectName(QStringLiteral("EditEpsilon"));

        formLayout->setWidget(5, QFormLayout::FieldRole, EditEpsilon);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_3);

        Interface->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Interface);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 695, 23));
        menuPelzaczek = new QMenu(menuBar);
        menuPelzaczek->setObjectName(QStringLiteral("menuPelzaczek"));
        Interface->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Interface);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Interface->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Interface);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Interface->setStatusBar(statusBar);

        menuBar->addAction(menuPelzaczek->menuAction());
        menuPelzaczek->addAction(actionZaladuj_funkcje);
        menuPelzaczek->addAction(actionZakoncz);

        retranslateUi(Interface);

        QMetaObject::connectSlotsByName(Interface);
    } // setupUi

    void retranslateUi(QMainWindow *Interface)
    {
        Interface->setWindowTitle(QApplication::translate("Interface", "Interface", 0));
        actionZaladuj_funkcje->setText(QApplication::translate("Interface", "Za\305\202aduj funkcj\304\231", 0));
        actionZakoncz->setText(QApplication::translate("Interface", "Zako\305\204cz", 0));
        LoadFunction->setText(QApplication::translate("Interface", "Za\305\202aduj funkcj\304\231", 0));
        Start->setText(QApplication::translate("Interface", "Start", 0));
        Pauza->setText(QApplication::translate("Interface", "Pauza", 0));
        Stop->setText(QApplication::translate("Interface", "Stop", 0));
        label->setText(QApplication::translate("Interface", " wsp. beta", 0));
#ifndef QT_NO_TOOLTIP
        EditBeta->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\262.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        EditAlfa->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\261</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("Interface", " wsp. alfa", 0));
#ifndef QT_NO_TOOLTIP
        IterationAmount->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wybierz liczb\304\231 iteracji algorytmu.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("Interface", " Liczba iteracji", 0));
#ifndef QT_NO_TOOLTIP
        EditGamma->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \311\243.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("Interface", " wsp. gamma", 0));
#ifndef QT_NO_TOOLTIP
        EditEpsilon->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\265.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("Interface", " wsp. epsilon", 0));
        menuPelzaczek->setTitle(QApplication::translate("Interface", "Pe\305\202zaczek", 0));
    } // retranslateUi

};

namespace Ui {
    class Interface: public Ui_Interface {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTERFACE_H
