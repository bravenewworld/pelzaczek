#-------------------------------------------------
#
# Project created by QtCreator 2015-04-13T15:28:28
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Pelzaczek
TEMPLATE = app


win32 {
QMAKE_CXXFLAGS += /bigobj
}


SOURCES += main.cpp\
        interface.cpp \
    qcustomplot.cpp \
    simpleks.cpp \
    parser.cpp \
    help.cpp

HEADERS  += interface.h \
    qcustomplot.h \
    simpleks.h \
    exprtk/exprtk.hpp \
    parser.h \
    help.h

FORMS    += interface.ui \
    parser.ui \
    help.ui
