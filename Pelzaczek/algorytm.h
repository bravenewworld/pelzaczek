#ifndef ALGORYTM_H
#define ALGORYTM_H

#include <QDialog>
#include "sympleks.h"
#include <QVector>
#include <QDebug>
#include "exprtk.hpp"


class algorytm: public QDialog
{
    Q_OBJECT
    struct Point
    {
        int point_index;
        QVector <double> coords;
        double value;
    };

public:
    explicit algorytm(QWidget *parent = 0);
    algorytm(double a, double b, double g, Sympleks* symp, int i);
    ~algorytm();
    std::string expr_string;
    Sympleks* Symp;
    QVector <double> symp_value;
    float alfa, beta, gamma;
    Point aH, aSH, aL, aR, aE, aC, sc;
    int iter;
    int iter_max;

    template <typename T>
    void Nelder_Meade(double eps);

//    template <typename T>
//    void function_evaluation();
//    template <typename T>
//    void symp_value_evaluation();
//    void symetry_center();
//    bool expand();
//    bool contract_in();
//    bool contract_out();
//    void reduct();




};

#endif // ALGORYTM_H
