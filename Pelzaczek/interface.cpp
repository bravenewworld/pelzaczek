#include "interface.h"
#include "ui_interface.h"

Interface::Interface(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Interface)
{
    setWindowTitle(tr("Optymalizacja funkcji nieliniowej metodą Neldera-Meada"));
    resize(400, 400);
    ui->setupUi(this);

    ui->StepSlider->setTracking(0);
    ui->StepSlider->setSingleStep(1);
    ui->IterationAmount->setRange(1,1000);
    ui->IterationAmount->setWrapping(true);
    ui->IterationAmount->stepBy(1);
    //    QObject::connect(this, this->Start_algorithm(ui->StepSlider->value()),
    //                     this->Symp2D, this->Symp2D.on_Start);
    //    QObject::connect(this, this->Stop_algorithm(ui->StepSlider->value()),
    //                      this->Symp2D, this->Symp2D.on_Stop);
    connect(&Symp2D, SIGNAL(did_contraction()),
            this, SLOT(on_contraction()));
    connect(&Symp2D, SIGNAL(did_reduction()),
            this, SLOT(on_reduction()));
    connect(&Symp2D, SIGNAL(did_expansion()),
            this, SLOT(on_expansion()));
    connect(&okienko, SIGNAL(FunctionLoaded(std::string)),
            this, SLOT(on_FunctionLoaded(std::string)));
}

Interface::~Interface()
{
    delete ui;
}


void Interface::on_StepSlider_sliderMoved(int position)
{
    ui->wykres->replot();
}

void Interface::on_LoadFunction_clicked()
{
    okienko.exec();

}

void Interface::on_Start_clicked()
{
    emit Start_algorithm(ui->StepSlider->value());
    parsowanie<double>();
   // ui->wykres->replot();
}

void Interface::on_Pauza_clicked()
{

}

void Interface::on_Stop_clicked()
{
    emit Stop_algorithm(ui->StepSlider->value());
}


void Interface::on_EditAlfa_textEdited(const QString &arg1)
{
    this->alfa=arg1.toFloat();
    this->Symp2D.a=arg1.toFloat();
}

void Interface::on_EditBeta_textEdited(const QString &arg1)
{
    this->beta=arg1.toFloat();
    this->Symp2D.b=arg1.toFloat();
}

void Interface::on_EditGamma_textEdited(const QString &arg1)
{
    this->gamma=arg1.toFloat();
    this->Symp2D.c=arg1.toFloat();
}

void Interface::on_EditEpsilon_textEdited(const QString &arg1)
{
    this->eps=arg1.toFloat();
}

void Interface::on_IterationAmount_valueChanged(int arg1)
{
    this->iter_amount=arg1;
}

void Interface::on_reflection()
{

}

void Interface::on_expansion()
{

}

void Interface::on_contraction()
{

}

void Interface::on_reduction()
{

}

void Interface::on_FunctionLoaded(std::string function)
{
  this->expr_string=function;
  QString func;

  //  qDebug()<< "podano " << func.fromStdString(function);
  //  qDebug()<< "zapisano " << func.fromStdString(expr_string);

    if (func.fromStdString(function).contains("x2"))
    {
        this->Type=3;
        ui->min_x1->setEnabled(true);
        ui->min_x2->setEnabled(true);
        ui->max_x1->setEnabled(true);
        ui->max_x2->setEnabled(true);
       // qDebug()<<"Typ" <<Type;
    }
    else if (func.fromStdString(function).contains("x1"))
    {
        this->Type=2;
        ui->min_x1->setEnabled(true);
        ui->max_x1->setEnabled(true);
       // qDebug()<<"Typ" <<Type;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Błędnie wprowadzona formuła.\nNie znaleziono zmiennej x1.");
        msgBox.exec();
    }

}

template <typename T>
/*!
 * \brief Interface::parsowanie funkcja wywoływana przy zatwierdzeniu wpisanej funkcji, odpowiada za przerobienie
 * wpisanego tekstu na wyrażenie matematyczne
 * \param expr tekst wpisany w okno
 */
void Interface::parsowanie()
{
      typedef exprtk::symbol_table<T> symbol_table_t;
      typedef exprtk::expression<T>     expression_t;
      typedef exprtk::parser<T>             parser_t;

    T x1;
    T x2;
    T x3;
    T x4;
    T x5;
    T y;

    symbol_table_t symbol_table;
    symbol_table.add_variable("x1",x1);
    symbol_table.add_variable("x2",x2);
    symbol_table.add_variable("x3",x3);
    symbol_table.add_variable("x4",x4);
    symbol_table.add_variable("x5",x5);
    symbol_table.add_variable("y",y);
    symbol_table.add_constants();

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(this->expr_string,expression);

    if (this->Type==2)
    {
        int length=abs(ui->max_x1->value() - ui->min_x1->value());
        double min_y=0;
        double max_y=0;
        QVector<double> x(length);
        QVector<double> y(length); // initialize with entries 0..100
        for (int i=0; i<length; ++i)
        {
          x[i] = ui->min_x1->value()+i;
          x1=ui->min_x1->value()+i;
          y[i] = expression.value();
          qDebug()<<y[i];
          if(y[i]<min_y)    { min_y=y[i]; }
          else if(y[i]>max_y)    { max_y=y[i]; }

        }
        // create graph and assign data to it:
        ui->wykres->addGraph();
        ui->wykres->graph(0)->setData(x, y);
        // give the axes some labels:
        ui->wykres->xAxis->setLabel("x");
        ui->wykres->yAxis->setLabel("y");
        // set axes ranges, so we see all data:
        ui->wykres->xAxis->setRange(ui->max_x1->value(), ui->min_x1->value());
        ui->wykres->yAxis->setRange(min_y, max_y);
        ui->wykres->replot();
    }

    if (this->Type==3)
    {
        int length1=abs(ui->max_x1->value() - ui->min_x1->value());
        int length2=abs(ui->max_x2->value() - ui->min_x2->value());

        // configure axis rect:
        ui->wykres->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
        ui->wykres->axisRect()->setupFullAxesBox(true);
        ui->wykres->xAxis->setLabel("x1");
        ui->wykres->yAxis->setLabel("x2");

        // set up the QCPColorMap:
        QCPColorMap *colorMap = new QCPColorMap(ui->wykres->xAxis, ui->wykres->yAxis);
        ui->wykres->addPlottable(colorMap);
     //   int nx = 200;
     //   int ny = 200;
        colorMap->data()->setSize(length1, length2); // we want the color map to have nx * ny data points
        colorMap->data()->setRange(QCPRange(ui->min_x1->value(), ui->max_x1->value()),
                                   QCPRange(ui->min_x2->value(), ui->max_x2->value())); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
        // now we assign some data, by accessing the QCPColorMapData instance of the color map:
        double y;
        for (int i=0; i<length1; ++i)
        {
          for (int j=0; j<length2; ++j)
          {
            colorMap->data()->cellToCoord(i, j, &x1, &x2);
            y=expression.value();
            colorMap->data()->setCell(i, j, y);
          }
        }

        // add a color scale:
        QCPColorScale *colorScale = new QCPColorScale(ui->wykres);
        ui->wykres->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
        colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorMap->setColorScale(colorScale); // associate the color map with the color scale
        colorScale->axis()->setLabel("Wartość w osi prostopadłej do ekranu");

        // set the color gradient of the color map to one of the presets:
        colorMap->setGradient(QCPColorGradient::gpPolar);
        // we could have also created a QCPColorGradient instance and added own colors to
        // the gradient, see the documentation of QCPColorGradient for what's possible.

        // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
        colorMap->rescaleDataRange();

        // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->wykres);
        ui->wykres->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

        // rescale the key (x) and value (y) axes so the whole color map is visible:
        ui->wykres->rescaleAxes();
    }

}
