/*! \file Plik zawiera metody służące rysowaniu wykresu przebiegu działania algorytmu.
 */
#ifndef WYKRES_H
#define WYKRES_H

#include <QWidget>
/*!
 * \brief The Wykres class
 */
class Wykres : public QWidget
{
    Q_OBJECT
public:
    explicit Wykres(QWidget *parent = 0);
    ~Wykres();

signals:

public slots:

protected:

};

#endif // WYKRES_H
