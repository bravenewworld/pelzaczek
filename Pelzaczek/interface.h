#ifndef INTERFACE_H
#define INTERFACE_H
#include <QObject>
#include <QMainWindow>
#include <QDebug>
#include "rysowanie.h"
#include "qcustomplot.h"
#include "simpleks.h"
#include "parser.h"
#include "exprtk/exprtk.hpp"

namespace Ui {
class Interface;
}

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    explicit Interface(QWidget *parent = 0);
    ~Interface();
    double eps;
    double alfa;
    double beta;
    double gamma;
    int iter_amount;
    int Type;
    Sympleks2D Symp2D;
    Sympleks3D Symp3D;
    std::string expr_string;
    Parser okienko;

    template <typename T>
    void parsowanie();



private slots:
    void on_StepSlider_sliderMoved(int position);

    void on_LoadFunction_clicked();

    void on_Start_clicked();

    void on_Pauza_clicked();

    void on_Stop_clicked();

    void on_EditAlfa_textEdited(const QString &arg1);

    void on_EditBeta_textEdited(const QString &arg1);

    void on_EditGamma_textEdited(const QString &arg1);

    void on_EditEpsilon_textEdited(const QString &arg1);

    void on_IterationAmount_valueChanged(int arg1);

    void on_reflection();

    void on_expansion();

    void on_contraction();

    void on_reduction();

    void on_FunctionLoaded(std::string function);

private:
    Ui::Interface *ui;

signals:
    void Stop_algorithm(int iter);
    void Start_algorithm(int iter);

};

#endif // INTERFACE_H
