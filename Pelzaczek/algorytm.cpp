#include "algorytm.h"

algorytm::algorytm(QWidget *parent) :
QDialog(parent)
{

}

algorytm::algorytm(double a, double b, double g, Sympleks* symp, int i)
{
    this->alfa=a;
    this->beta=b;
    this->gamma=g;
    this->Symp=symp;
    iter=0;
    iter_max=i;
}

algorytm::~algorytm()
{

}

template <typename T>
/**
 * @brief algorytm::Nelder_Meade
 * @param eps
 */
void algorytm::Nelder_Meade(double eps)
{
    bool expand_flag=false;
    bool contract_in_flag=false;
    bool contract_out_flag=false;
    double val;
    QVector<double> x;


    typedef exprtk::symbol_table<T> symbol_table_t;
    typedef exprtk::expression<T>     expression_t;
    typedef exprtk::parser<T>             parser_t;

    T x1;
    T x2;
    T x3;
    T x4;
    T x5;

    symbol_table_t symbol_table;
    expression_t expression;
    parser_t parser;

    do
    {
        // function_evaluation();
        symbol_table.add_variable("x1",x1);
        if(Symp->num_of_coordinates>=3)
        {
            symbol_table.add_variable("x2",x2);
            if(Symp->num_of_coordinates>=4)
            {
                symbol_table.add_variable("x3",x3);
                if(Symp->num_of_coordinates>=5)
                {
                    symbol_table.add_variable("x4",x4);
                    if(Symp->num_of_coordinates==6)
                    {
                        symbol_table.add_variable("x5",x5);
                    }
                }
            }
        }

        symbol_table.add_constants();

        expression.register_symbol_table(symbol_table);

        parser.compile(this->expr_string, expression);

        // symp_value_evaluation();

        symp_value.clear();

        for (int i=0; i<this->Symp->num_of_points; i++)
        {
            x.clear();

            x1=Symp->iteration.value(iter).value(i).value(0);
            x.append(Symp->iteration.value(iter).value(i).value(0));
            if(Symp->num_of_coordinates>=3)
            {
                x2=Symp->iteration.value(iter).value(i).value(1);
                x.append(Symp->iteration.value(iter).value(i).value(1));
                if(Symp->num_of_coordinates>=4)
                {
                    x3=Symp->iteration.value(iter).value(i).value(2);
                    x.append(Symp->iteration.value(iter).value(i).value(2));
                    if(Symp->num_of_coordinates>=5)
                    {
                        x4=Symp->iteration.value(iter).value(i).value(3);
                        x.append(Symp->iteration.value(iter).value(i).value(3));
                        if(Symp->num_of_coordinates==6)
                        {
                            x5=Symp->iteration.value(iter).value(i).value(4);
                            x.append(Symp->iteration.value(iter).value(i).value(4));
                        }
                    }
                }
            }
            val=expression.value();
            symp_value.append(val);


            if (iter==0 && i==0)
            {
                aH.point_index=0;
                aSH.point_index=0;
                aL.point_index=0;
                aH.value=val;
                aSH.value=val;
                aL.value=val;
                aH.coords=x;
                aSH.coords=x;
                aL.coords=x;
            }

            if (val>aH.value)
            {
                aSH.value=aH.value;
                aSH.point_index=aH.point_index;
                aSH.coords=aH.coords;
                aH.value=val;
                aH.point_index=i;
                aH.coords=x;
            }

            if (val>aSH.value && val<aH.value)
            {
                aSH.value=val;
                aSH.point_index=i;
                aSH.coords=x;
            }
            if (val<aL)
            {
                aL.value=val;
                aL.point_index=i;
                aL.coords=x;
            }
        }


        //symetry_center();
        double tmp1=0;
        double tmp2=0;
        double tmp3=0;
        double tmp4=0;
        double tmp5=0;

        for(int i=0; i<Symp->num_of_points; i++)
        {
            if (i!=aH.point_index)
            {
                tmp1+=Symp->iteration.value(iter).value(i).value(0);

                if(Symp->num_of_coordinates>=3)
                {
                    tmp2+=Symp->iteration.value(iter).value(i).value(1);

                    if(Symp->num_of_coordinates>=4)
                    {
                        tmp3+=Symp->iteration.value(iter).value(i).value(2);

                        if(Symp->num_of_coordinates>=5)
                        {
                            tmp4+=Symp->iteration.value(iter).value(i).value(3);
                            if(Symp->num_of_coordinates==6)
                            {
                                tmp5+=Symp->iteration.value(iter).value(i).value(4);
                            }
                        }
                    }
                }
            }
        }
        tmp1=tmp1/(Symp->num_of_points-1);
        tmp2=tmp2/(Symp->num_of_points-1);
        tmp3=tmp3/(Symp->num_of_points-1);
        tmp4=tmp4/(Symp->num_of_points-1);
        tmp5=tmp5/(Symp->num_of_points-1);

        sc.coords.clear();
        sc.coords.append(tmp1);
        sc.coords.append(tmp2);
        sc.coords.append(tmp3);
        sc.coords.append(tmp4);
        sc.coords.append(tmp5);

        // aR=sc+(sc-aH)
        aR.coords.clear();
        aR.coords.append(sc.coords.value(0)+(sc.coords.value(0)-aH.coords.value(0)));
        x1=aR.coords.value(0);
        if(Symp->num_of_coordinates>=3)
        {
            aR.coords.append(sc.coords.value(1)+(sc.coords.value(1)-aH.coords.value(1)));
            x2=aR.coords.value(1);
            if(Symp->num_of_coordinates>=4)
            {
                aR.coords.append(sc.coords.value(2)+(sc.coords.value(2)-aH.coords.value(2)));
                x3=aR.coords.value(2);
                if(Symp->num_of_coordinates>=5)
                {
                    aR.coords.append(sc.coords.value(3)+(sc.coords.value(3)-aH.coords.value(3)));
                    x4=aR.coords.value(3);
                    if(Symp->num_of_coordinates==6)
                    {
                        aR.coords.append(sc.coords.value(4)+(sc.coords.value(4)-aH.coords.value(4)));
                        x5=aR.coords.value(4);
                    }
                }
            }
        }
        aR.value=expression.value();


        //Przygototwanie nowej iteracji
        Symp->iteration.resize(iter+1);
        Symp->iteration.insert(iter+1, Symp->iteration.value(iter));

        if(aR.value<aL.value)
        {
            // expand_flag=expand();
            aE.coords.append(sc.coords.value(0)+gamma*(aR.coords.value(0)-sc.coords.value(0)));
            x1=aE.coords.value(0);
            if(Symp->num_of_coordinates>=3)
            {
                aE.coords.append(sc.coords.value(1)+gamma*(aR.coords.value(1)-sc.coords.value(1)));
                x2=aE.coords.value(1);
                if(Symp->num_of_coordinates>=4)
                {
                    aE.coords.append(sc.coords.value(2)+gamma*(aR.coords.value(2)-sc.coords.value(2)));
                    x3=aE.coords.value(2);
                    if(Symp->num_of_coordinates>=5)
                    {
                        aE.coords.append(sc.coords.value(3)+gamma*(aR.coords.value(3)-sc.coords.value(3)));
                        x4=aE.coords.value(3);
                        if(Symp->num_of_coordinates==6)
                        {
                            aE.coords.append(sc.coords.value(4)+gamma*(aR.coords.value(4)-sc.coords.value(4)));
                            x5=aE.coords.value(4);
                        }
                    }
                }
            }
            aE.value=expression.value();

            if(aE.value<aR.value)
            {
                aE.point_index=aR.point_index;
                Symp->iteration.value(iter+1).value(aR.point_index).insert(0, aE.coords.value(0));
                if(Symp->num_of_coordinates>=3)
                {
                    Symp->iteration.value(iter+1).value(aR.point_index).insert(1, aE.coords.value(1));
                    if(Symp->num_of_coordinates>=4)
                    {
                        Symp->iteration.value(iter+1).value(aR.point_index).insert(2, aE.coords.value(2));
                        if(Symp->num_of_coordinates>=5)
                        {
                            Symp->iteration.value(iter+1).value(aR.point_index).insert(3, aE.coords.value(3));
                            if(Symp->num_of_coordinates==6)
                            {
                                Symp->iteration.value(iter+1).value(aR.point_index).insert(4, aE.coords.value(4));
                            }
                        }
                    }
                }
                expand_flag=true;
            }

        }

        if(aR.value<aSH.value)
        {
            if(aR.value>aH.value)
            {
                //contract_in_flag=contract_in();
                aC.coords.append(sc.coords.value(0)+beta*(aH.coords.value(0)-sc.coords.value(0)));
                x1=aC.coords.value(0);
                if(Symp->num_of_coordinates>=3)
                {
                    aC.coords.append(sc.coords.value(1)+beta*(aH.coords.value(1)-sc.coords.value(1)));
                    x2=aC.coords.value(1);
                    if(Symp->num_of_coordinates>=4)
                    {
                        aC.coords.append(sc.coords.value(2)+beta*(aH.coords.value(2)-sc.coords.value(2)));
                        x3=aC.coords.value(2);
                        if(Symp->num_of_coordinates>=5)
                        {
                            aC.coords.append(sc.coords.value(3)+beta*(aH.coords.value(3)-sc.coords.value(3)));
                            x4=aC.coords.value(3);
                            if(Symp->num_of_coordinates==6)
                            {
                                aC.coords.append(sc.coords.value(4)+beta*(aH.coords.value(4)-sc.coords.value(4)));
                                x5=aC.coords.value(4);
                            }
                        }
                    }
                }
                aC.value=expression.value();

                if(aC.value<aH.value)
                {
                    aC.point_index=aH.point_index;

                    Symp->iteration.value(iter+1).value(aH.point_index).insert(0, aC.coords.value(0));
                    if(Symp->num_of_coordinates>=3)
                    {
                        Symp->iteration.value(iter+1).value(aH.point_index).insert(1, aC.coords.value(1));
                        if(Symp->num_of_coordinates>=4)
                        {
                            Symp->iteration.value(iter+1).value(aH.point_index).insert(2, aC.coords.value(2));
                            if(Symp->num_of_coordinates>=5)
                            {
                                Symp->iteration.value(iter+1).value(aH.point_index).insert(3, aC.coords.value(3));
                                if(Symp->num_of_coordinates==6)
                                {
                                    Symp->iteration.value(iter+1).value(aH.point_index).insert(4, aC.coords.value(4));
                                }
                            }
                        }
                    }
                    contract_in_flag=true;
                }
            }
            if(aR.value<aH.value)
            {
                //contract_out_flag=contract_out();
                aC.coords.append(sc.coords.value(0)+beta*(aR.coords.value(0)-sc.coords.value(0)));
                x1=aC.coords.value(0);
                if(Symp->num_of_coordinates>=3)
                {
                    aC.coords.append(sc.coords.value(1)+beta*(aR.coords.value(1)-sc.coords.value(1)));
                    x2=aC.coords.value(1);
                    if(Symp->num_of_coordinates>=4)
                    {
                        aC.coords.append(sc.coords.value(2)+beta*(aR.coords.value(2)-sc.coords.value(2)));
                        x3=aC.coords.value(2);
                        if(Symp->num_of_coordinates>=5)
                        {
                            aC.coords.append(sc.coords.value(3)+beta*(aR.coords.value(3)-sc.coords.value(3)));
                            x4=aC.coords.value(3);
                            if(Symp->num_of_coordinates==6)
                            {
                                aC.coords.append(sc.coords.value(4)+beta*(aR.coords.value(4)-sc.coords.value(4)));
                                x5=aC.coords.value(4);
                            }
                        }
                    }
                }
                aC.value=expression.value();

                if(aC.value<aH.value)
                {

                    aC.point_index=aH.point_index;

                    Symp->iteration.value(iter+1).value(aH.point_index).insert(0, aC.coords.value(0));
                    if(Symp->num_of_coordinates>=3)
                    {
                        Symp->iteration.value(iter+1).value(aH.point_index).insert(1, aC.coords.value(1));
                        if(Symp->num_of_coordinates>=4)
                        {
                            Symp->iteration.value(iter+1).value(aH.point_index).insert(2, aC.coords.value(2));
                            if(Symp->num_of_coordinates>=5)
                            {
                                Symp->iteration.value(iter+1).value(aH.point_index).insert(3, aC.coords.value(3));
                                if(Symp->num_of_coordinates==6)
                                {
                                    Symp->iteration.value(iter+1).value(aH.point_index).insert(4, aC.coords.value(4));
                                }
                            }
                        }
                    }
                    contract_out_flag=true;
                }
            }
        }

        if(expand_flag==false && contract_in_flag==false && contract_out_flag==false)
        {
            //reduct();
            Sympleks::coordinate temp_coord;
            Sympleks::point temp_point;

            double aTemp;
            for (int i=0; i<Symp->num_of_points; i++)
            {
                temp_coord.clear();
                aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(0)-aL.value);
                temp_coord.append(aTemp);

                if(Symp->num_of_coordinates>=3)
                {
                    aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(1)-aL.value);
                    temp_coord.append(aTemp);

                    if(Symp->num_of_coordinates>=4)
                    {
                        aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(2)-aL.value);
                        temp_coord.append(aTemp);

                        if(Symp->num_of_coordinates>=5)
                        {
                            aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(3)-aL.value);
                            temp_coord.append(aTemp);

                            if(Symp->num_of_coordinates==6)
                            {
                                aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(4)-aL.value);
                                temp_coord.append(aTemp);
                            }
                        }
                    }
                }
                temp_point.append(temp_coord);
            }

            Symp->iteration.insert(iter+1, temp_point);

        }

    } while(abs(aL.value-aH.value)>eps && iter<iter_max);

    if(iter<iter_max)
    {
        qDebug()<<"Znaleziono minimum funkcji!\n"
               <<"Liczba iteracji: " << iter <<endl
              <<"Wartość funkcji: " << aL.value <<endl
             <<"Położenie: " << aL.coords;
    }
    else
    {
        qDebug()<<"Osiągnięto maksymalną liczbę iteracji nie znajdując minimum funkcji.";
    }

}


//template <typename T>
//void algorytm::symp_value_evaluation()
//{
//    double val;
//    QVector<double> x;
//    symp_value.clear();

//    for (int i=0; i<this->Symp->num_of_points; i++)
//    {
//        x.clear();

//        x1=Symp->iteration.value(iter).value(i).value(0);
//        x.append(Symp->iteration.value(iter).value(i).value(0));
//        if(Symp->num_of_coordinates>=3)
//        {
//            x2=Symp->iteration.value(iter).value(i).value(1);
//            x.append(Symp->iteration.value(iter).value(i).value(1));
//            if(Symp->num_of_coordinates>=4)
//            {
//                x3=Symp->iteration.value(iter).value(i).value(2);
//                x.append(Symp->iteration.value(iter).value(i).value(2));
//                if(Symp->num_of_coordinates>=5)
//                {
//                    x4=Symp->iteration.value(iter).value(i).value(3);
//                    x.append(Symp->iteration.value(iter).value(i).value(3));
//                    if(Symp->num_of_coordinates==6)
//                    {
//                        x5=Symp->iteration.value(iter).value(i).value(4);
//                        x.append(Symp->iteration.value(iter).value(i).value(4));
//                    }
//                }
//            }
//        }
//        val=expression.value();
//        symp_value.append(val);


//        if (iter==0 && i==0)
//        {
//            aH.point_index=0;
//            aSH.point_index=0;
//            aL.point_index=0;
//            aH.value=val;
//            aSH.value=val;
//            aL.value=val;
//            aH.coords=x;
//            aSH.coords=x;
//            aL.coords=x;
//        }

//        if (val>aH.value)
//        {
//            aSH.value=aH.value;
//            aSH.point_index=aH.point_index;
//            aSH.coords=aH.coords;
//            aH.value=val;
//            aH.point_index=i;
//            aH.coords=x;
//        }

//        if (val>aSH.value && val<aH.value)
//        {
//            aSH.value=val;
//            aSH.point_index=i;
//            aSH.coords=x;
//        }
//        if (val<aL)
//        {
//            aL.value=val;
//            aL.point_index=i;
//            aL.coords=x;
//        }
//    }

//}

//template <typename T>
//void algorytm::function_evaluation()
//{

//    symbol_table.add_variable("x1",x1);
//    if(Symp->num_of_coordinates>=3)
//    {
//        symbol_table.add_variable("x2",x2);
//        if(Symp->num_of_coordinates>=4)
//        {
//            symbol_table.add_variable("x3",x3);
//            if(Symp->num_of_coordinates>=5)
//            {
//                symbol_table.add_variable("x4",x4);
//                if(Symp->num_of_coordinates==6)
//                {
//                    symbol_table.add_variable("x5",x5);
//                }
//            }
//        }
//    }

//  symbol_table.add_constants();

//  expression.register_symbol_table(symbol_table);

//  parser.compile(this->expr_string, expression);


//}

//void algorytm::symetry_center()
//{
//    double tmp1=0;
//    double tmp2=0;
//    double tmp3=0;
//    double tmp4=0;
//    double tmp5=0;

//    for(int i=0; i<Symp->num_of_points; i++)
//    {
//        if (i!=aH.point_index)
//        {
//            tmp1+=Symp->iteration.value(iter).value(i).value(0);

//            if(Symp->num_of_coordinates>=3)
//            {
//                tmp2+=Symp->iteration.value(iter).value(i).value(1);

//                if(Symp->num_of_coordinates>=4)
//                {
//                    tmp3+=Symp->iteration.value(iter).value(i).value(2);

//                    if(Symp->num_of_coordinates>=5)
//                    {
//                        tmp4+=Symp->iteration.value(iter).value(i).value(3);
//                        if(Symp->num_of_coordinates==6)
//                        {
//                            tmp5+=Symp->iteration.value(iter).value(i).value(4);
//                        }
//                    }
//                }
//            }
//        }
//    }
//    tmp1=tmp1/(Symp->num_of_points-1);
//    tmp2=tmp2/(Symp->num_of_points-1);
//    tmp3=tmp3/(Symp->num_of_points-1);
//    tmp4=tmp4/(Symp->num_of_points-1);
//    tmp5=tmp5/(Symp->num_of_points-1);

//    sc.coords.clear();
//    sc.coords.append(tmp1);
//    sc.coords.append(tmp2);
//    sc.coords.append(tmp3);
//    sc.coords.append(tmp4);
//    sc.coords.append(tmp5);

//    // aR=sc+(sc-aH)
//    aR.coords.clear();
//      aR.coords.append(sc.coords.value(0)+(sc.coords.value(0)-aH.coords.value(0)));
//      x1=aR.coords.value(0);
//        if(Symp->num_of_coordinates>=3)
//        {
//            aR.coords.append(sc.coords.value(1)+(sc.coords.value(1)-aH.coords.value(1)));
//            x2=aR.coords.value(1);
//            if(Symp->num_of_coordinates>=4)
//            {
//                aR.coords.append(sc.coords.value(2)+(sc.coords.value(2)-aH.coords.value(2)));
//                x3=aR.coords.value(2);
//                if(Symp->num_of_coordinates>=5)
//                {
//                    aR.coords.append(sc.coords.value(3)+(sc.coords.value(3)-aH.coords.value(3)));
//                   x4=aR.coords.value(3);
//                    if(Symp->num_of_coordinates==6)
//                    {
//                        aR.coords.append(sc.coords.value(4)+(sc.coords.value(4)-aH.coords.value(4)));
//                        x5=aR.coords.value(4);
//                    }
//                }
//            }
//        }
//        aR.value=expression.value();
//}

//bool algorytm::expand()
//{
//    aE.coords.append(sc.coords.value(0)+gamma*(aR.coords.value(0)-sc.coords.value(0)));
//    x1=aE.coords.value(0);
//      if(Symp->num_of_coordinates>=3)
//      {
//          aE.coords.append(sc.coords.value(1)+gamma*(aR.coords.value(1)-sc.coords.value(1)));
//          x2=aE.coords.value(1);
//          if(Symp->num_of_coordinates>=4)
//          {
//              aE.coords.append(sc.coords.value(2)+gamma*(aR.coords.value(2)-sc.coords.value(2)));
//              x3=aE.coords.value(2);
//              if(Symp->num_of_coordinates>=5)
//              {
//                  aE.coords.append(sc.coords.value(3)+gamma*(aR.coords.value(3)-sc.coords.value(3)));
//                  x4=aE.coords.value(3);
//                  if(Symp->num_of_coordinates==6)
//                  {
//                      aE.coords.append(sc.coords.value(4)+gamma*(aR.coords.value(4)-sc.coords.value(4)));
//                      x5=aE.coords.value(4);
//                  }
//              }
//          }
//      }
//      aE.value=expression.value();

//      if(aE.value<aR.value)
//      {
//          aE.point_index=aR.point_index;
//          Symp->iteration.value(iter+1).value(aR.point_index).insert(0, aE.coords.value(0));
//          if(Symp->num_of_coordinates>=3)
//          {
//              Symp->iteration.value(iter+1).value(aR.point_index).insert(1, aE.coords.value(1));
//              if(Symp->num_of_coordinates>=4)
//              {
//                  Symp->iteration.value(iter+1).value(aR.point_index).insert(2, aE.coords.value(2));
//                  if(Symp->num_of_coordinates>=5)
//                  {
//                      Symp->iteration.value(iter+1).value(aR.point_index).insert(3, aE.coords.value(3));
//                      if(Symp->num_of_coordinates==6)
//                      {
//                          Symp->iteration.value(iter+1).value(aR.point_index).insert(4, aE.coords.value(4));
//                      }
//                  }
//              }
//          }
//          return 1;
//      }
//      else
//      {
//          return 0;
//      }
//}

//bool algorytm::contract_in()
//{
//    aC.coords.append(sc.coords.value(0)+beta*(aH.coords.value(0)-sc.coords.value(0)));
//    x1=aC.coords.value(0);
//      if(Symp->num_of_coordinates>=3)
//      {
//          aC.coords.append(sc.coords.value(1)+beta*(aH.coords.value(1)-sc.coords.value(1)));
//          x2=aC.coords.value(1);
//          if(Symp->num_of_coordinates>=4)
//          {
//              aC.coords.append(sc.coords.value(2)+beta*(aH.coords.value(2)-sc.coords.value(2)));
//              x3=aC.coords.value(2);
//              if(Symp->num_of_coordinates>=5)
//              {
//                  aC.coords.append(sc.coords.value(3)+beta*(aH.coords.value(3)-sc.coords.value(3)));
//                  x4=aC.coords.value(3);
//                  if(Symp->num_of_coordinates==6)
//                  {
//                      aC.coords.append(sc.coords.value(4)+beta*(aH.coords.value(4)-sc.coords.value(4)));
//                      x5=aC.coords.value(4);
//                  }
//              }
//          }
//      }
//      aC.value=expression.value();

//      if(aC.value<aH.value)
//      {
//          aC.point_index=aH.point_index;

//          Symp->iteration.value(iter+1).value(aH.point_index).insert(0, aC.coords.value(0));
//          if(Symp->num_of_coordinates>=3)
//          {
//              Symp->iteration.value(iter+1).value(aH.point_index).insert(1, aC.coords.value(1));
//              if(Symp->num_of_coordinates>=4)
//              {
//                  Symp->iteration.value(iter+1).value(aH.point_index).insert(2, aC.coords.value(2));
//                  if(Symp->num_of_coordinates>=5)
//                  {
//                      Symp->iteration.value(iter+1).value(aH.point_index).insert(3, aC.coords.value(3));
//                      if(Symp->num_of_coordinates==6)
//                      {
//                          Symp->iteration.value(iter+1).value(aH.point_index).insert(4, aC.coords.value(4));
//                      }
//                  }
//              }
//          }
//          return 1;
//      }
//      else
//      {
//          return 0;
//      }
//}

//bool algorytm::contract_out()
//{
//    aC.coords.append(sc.coords.value(0)+beta*(aR.coords.value(0)-sc.coords.value(0)));
//    x1=aC.coords.value(0);
//      if(Symp->num_of_coordinates>=3)
//      {
//          aC.coords.append(sc.coords.value(1)+beta*(aR.coords.value(1)-sc.coords.value(1)));
//          x2=aC.coords.value(1);
//          if(Symp->num_of_coordinates>=4)
//          {
//              aC.coords.append(sc.coords.value(2)+beta*(aR.coords.value(2)-sc.coords.value(2)));
//              x3=aC.coords.value(2);
//              if(Symp->num_of_coordinates>=5)
//              {
//                  aC.coords.append(sc.coords.value(3)+beta*(aR.coords.value(3)-sc.coords.value(3)));
//                  x4=aC.coords.value(3);
//                  if(Symp->num_of_coordinates==6)
//                  {
//                      aC.coords.append(sc.coords.value(4)+beta*(aR.coords.value(4)-sc.coords.value(4)));
//                      x5=aC.coords.value(4);
//                  }
//              }
//          }
//      }
//      aC.value=expression.value();

//      if(aC.value<aH.value)
//      {

//          aC.point_index=aH.point_index;

//          Symp->iteration.value(iter+1).value(aH.point_index).insert(0, aC.coords.value(0));
//          if(Symp->num_of_coordinates>=3)
//          {
//              Symp->iteration.value(iter+1).value(aH.point_index).insert(1, aC.coords.value(1));
//              if(Symp->num_of_coordinates>=4)
//              {
//                  Symp->iteration.value(iter+1).value(aH.point_index).insert(2, aC.coords.value(2));
//                  if(Symp->num_of_coordinates>=5)
//                  {
//                      Symp->iteration.value(iter+1).value(aH.point_index).insert(3, aC.coords.value(3));
//                      if(Symp->num_of_coordinates==6)
//                      {
//                          Symp->iteration.value(iter+1).value(aH.point_index).insert(4, aC.coords.value(4));
//                      }
//                  }
//              }
//          }

//          return 1;
//      }
//      else
//      {
//          return 0;
//      }
//}

//void algorytm::reduct()
//{
//    Sympleks::coordinate temp_coord;
//    Sympleks::point temp_point;

//    double aTemp;
//    for (int i=0; i<Symp->num_of_points; i++)
//    {
//        temp_coord.clear();
//        aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(0)-aL.value);
//        temp_coord.append(aTemp);

//        if(Symp->num_of_coordinates>=3)
//        {
//            aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(1)-aL.value);
//            temp_coord.append(aTemp);

//            if(Symp->num_of_coordinates>=4)
//            {
//                aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(2)-aL.value);
//                temp_coord.append(aTemp);

//                if(Symp->num_of_coordinates>=5)
//                {
//                    aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(3)-aL.value);
//                    temp_coord.append(aTemp);

//                    if(Symp->num_of_coordinates==6)
//                    {
//                        aTemp=aL.value+alfa*(Symp->iteration.value(iter).value(i).value(4)-aL.value);
//                        temp_coord.append(aTemp);
//                    }
//                }
//            }
//        }
//        temp_point.append(temp_coord);
//    }

//        Symp->iteration.insert(iter+1, temp_point);

//}


