#include "simpleks.h"

using namespace std;

void Sympleks2D::on_Stop()
{

}

void Sympleks2D::on_Start()
{

}

/*!
 * \brief Sympleks2D::reflection    -> metoda obliczająca punkt odbicia
 * \param p_h                       -> punkt w którym funkcja przyjmuje największą wartość (najgorszy)
 * \param p                         -> środek symetrii simpleksu liczony z wyłączeniem punktu p_h
 * \return
 */
double Sympleks2D::reflection(double p_h, double p)
{
    emit did_reflection();
    return (1+a)*p - a*p_h;
}

/*!
 * \brief Sympleks2D::expansion     -> metoda obliczająca punkt ekspansji simpleksu
 * \param pp                        -> wyliczony punkt odbicia dla danego kroku algorytmu
 * \param p                         -> środek symetrii simpleksu liczony z wyłączeniem punktu p_h
 * \return
 */
double Sympleks2D::expansion(double pp, double p)
{
    emit did_expansion();
    return (1+c)*pp - c*p;
}

/*!
 * \brief Sympleks2D::contraction   -> metoda obliczająca punkt kontrakcji simplesku
 * \param p_h                       -> punkt w którym funkcja przyjmuje największą wartość (najgorszy)
 * \param p                         -> środek symetrii simpleksu liczony z wyłączeniem punktu p_h
 * \return
 */
double Sympleks2D::contraction(double p_h, double p)
{
    emit did_contraction();
    return b*p_h + (1-b)*p;
}

/*!
 * \brief Sympleks2D::reduction     -> metoda redukująca simpleks
 * \param index                     -> indeks w tablic x[] punktu w którym funkcja osiąga najmniejszą wartość (najlepszy)
 */
void Sympleks2D::reduction(short index)
{
    short i=0;
    for(i=0;i<3;i++)
        Sympleks2D::x_2D[i] = (Sympleks2D::x_2D[i]+Sympleks2D::x_2D[index])/2;
    emit did_reduction();
    return ;
}

/*!
 * \brief Sympleks2D::Sympleks2D    -> konstruktor klasy Sympleks2D, alokacja pamięci dla 3 - elementowych wektorów x i y
 */
Sympleks2D::Sympleks2D(QObject *parent):
    QObject(parent)
{
    x_2D = {1.1,2.2,3.3};
    y_2D = {1,1,1};

    srand( time(NULL) );
    x_2D[0]=(rand()%100-50);
    x_2D[1]=(rand()%100-50);
    x_2D[2]=(rand()%100-50);
    this->a=1;
    this->b=0.5;
    this->c=1;

}

/*!
 * \brief Sympleks2D::~Sympleks2D   -> destruktor klasy Sympleks2D
 */
Sympleks2D::~Sympleks2D()
{

}

/*!
 * \brief Sympleks2D::f             -> metoda obliczająca wartość funkcji w danym punkcie (do zastąpienia parserem)
 * \param q                         -> argument funkcji
 * \return
 */
double Sympleks2D::f(double q)
{
    return 5*q*q-4*q+4;
}

/*!
 * \brief Sympleks2D::maxSearcher   -> wyszukuje indeks argumentu dla którego wartość funkcji jest największa (najgorszy punkt)
 * \return
 */
short Sympleks2D::maxSearcher()
{
    double temp_max;
    short index_max=0;
    int i=0;
    temp_max=y_2D[0];
    for(i=1;i<3;i++)
    {
        if(y_2D[i] > temp_max)
        {
            temp_max=y_2D[i];
            index_max=i;
        }
    }
    return index_max;
}

/*!
 * \brief Sympleks2D::minSearcher   -> wyszukuje indeks argumentu dla którego wartość funkcji jest najmniejsza (najlepszy punkt)
 * \return
 */
short Sympleks2D::minSearcher()
{
    double temp_min;
    short index_min=0;
    int i=0;
    temp_min=y_2D[0];
    for(i=1;i<3;i++)
    {
        if(y_2D[i]<temp_min)
        {
            temp_min=y_2D[i];
            index_min=i;
        }
    }
    return index_min;
}

/*!
 * \brief Sympleks2D::Nelder_Meade  -> algorytm optymalizacji funkcji jednej zmiennej
 * \param eps                       -> parametr przerwania algorytmu
 */
void Sympleks2D::Nelder_Meade(double eps)
{
    short index_max=0;
    short index_min=0;
    int i=0;
    double centerSym=0;
    double fcenterSym=0;
    double suma=0;
    double tempo=0,tempe=0,tempk=0;
    double fo=0,fe=0,fk=0;
 //   double eps=0.00001;
    int iteracje=0;

    //Obliczenie wartosci funkcji w wierzcholkach
    for(i=0;i<3;i++)
        y_2D[i] = f(x_2D[i]);

    while(fabs(y_2D[0]-y_2D[1])>eps || fabs(y_2D[0]-y_2D[2])>eps || fabs(y_2D[1]-y_2D[2])>eps)
    {
          ///////PUNKT ZWROTNY ALOGRYTMU///////////
        //Obliczenie wartosci funkcji w wierzcholkach
        for(i=0;i<3;i++)
            y_2D[i] = f(x_2D[i]);

        for(i=0;i<3;i++)
            cout << x_2D[i]<<endl;
        cout<<endl;

        index_max=maxSearcher();
        index_min=minSearcher();

        //Obliczenie puntku środka symetrii simpleksu
        for(i=0;i<3;i++)
            suma=suma+x_2D[i];
        centerSym = (suma-x_2D[index_max])/2;
        fcenterSym = f(centerSym);

        tempo = reflection(x_2D[index_max],centerSym);
        fo=f(tempo);
        if(fo<y_2D[index_min])
        {
            tempe = expansion(tempo,centerSym);
            fe = f(tempe);
            if(fe<y_2D[index_min])
            {
                x_2D[index_max] = tempe;
            }
            else
            {
                x_2D[index_max] = tempo;
            }
            //jezeli nie jest spelniony warunek stopu wroc do punktu zwrotnego
            if((abs(y_2D[0]-y_2D[1])>eps || abs(y_2D[0]-y_2D[2])>eps || abs(y_2D[1]-y_2D[2])>eps))
                continue;
        }
        if(fo>y_2D[index_min])
        {
            if(fo<y_2D[index_max])
            {
                x_2D[index_max] = tempo;
            }
            tempk = contraction(x_2D[index_max],centerSym);
            fk = f(tempk);
            if(fk>=y_2D[index_max])
            {
                reduction(index_min);
            }
            else
            {
                x_2D[index_max] = tempk;
            }
            if(fo<y_2D[0] && fo<y_2D[1] && fo<y_2D[2])
            {
                x_2D[index_max] = tempo;
            }
        }
        iteracje++;
    }
    cout<<"*********WYNIK**********"<<endl;
    cout<<"Wartosc minimalna funkcji to: "<<y_2D[index_min]<< " dla argumentu: "<<x_2D[index_min]<<endl;
    cout<<"Liczba iteracji: "<<iteracje<<endl;
}

//----------------------Sympleks3D--------------------------------//


/*!
 * \brief Sympleks3D::reflection
 * \param p_h
 * \param p
 * \return
 */
double *Sympleks3D::reflection(double p_h1, double p_h2, double *p)
{
    static double tab[2];
    tab[0] = (1+a)*(*p)-a*p_h1;
    tab[1] = (1+a)*(*(p+1))-a*p_h2;
    return tab;
}

/*!
 * \brief Sympleks3D::expansion
 * \param pp
 * \param p
 * \return
 */
double* Sympleks3D::expansion(double pp1, double pp2, double* p)
{
    static double tab[2];
    tab[0] = (1+c)*pp1 - c*(*p);
    tab[1] = (1+c)*pp2 - c*(*(p+1));
    return tab;
}

/*!
 * \brief Sympleks3D::contraction
 * \param p_h
 * \param p
 * \return
 */
double *Sympleks3D::contraction(double p_h1, double p_h2, double *p)
{
    static double tab[2];
    tab[0] = b*p_h1 + (1-b)*(*p);
    tab[1] = b*p_h2 + (1-b)*(*(p+1));
    return tab;
}

/*!
 * \brief Sympleks3D::reduction
 * \param index
 */
void Sympleks3D::reduction(short index)
{
    short i=0;
    for(i=0;i<4;i++)
    {
        //x[i][0] = (x[i][0]+x[index][0])/2;
        //x[i][1] = (x[i][1]+x[index][1])/2;
        x1_3D[i] = (x1_3D[i]+x1_3D[index])/2;
        x2_3D[i] = (x2_3D[i]+x2_3D[index])/2;
    }
    return ;
}

/*!
 * \brief Sympleks3D::Sympleks3D    konstruktor klasy Sympleks3D, alokacja pamięci dla wektorów 4 elementowych x1,x2,y
 */
Sympleks3D::Sympleks3D(QObject *parent):
    QObject(parent)
{
    x1_3D = {1.1,2.2,3.3,4.4};
    x2_3D = {1.1,2.2,3.3,4.4};
    y_3D = {1,1,1,1};

    short i=0;
    srand( time(NULL) );
    for(i=0;i<4;i++)
    {
        x1_3D[i] = rand()%100 - 50;
        x2_3D[i] = rand()%100 - 50;
        //for(j=0;j<2;j++)
        //{
        //    x[i][j] = rand()%100 - 50;
        //}
    }
    this->a=2;
    this->b=1;
    this->c=2;
}

/*!
 * \brief Sympleks3D::~Sympleks3D
 */
Sympleks3D::~Sympleks3D()
{

}

/*!
 * \brief Sympleks3D::f
 * \param q
 * \param w
 * \return
 */
double Sympleks3D::f(double q, double w)
{
    //return 3*pow(q,4.0)-2/3*pow(w,3.0)+2*pow(q,2)*w-2*pow(q,2)+pow(w,2);

    return q*q+w*w;
    //return q*q+q+w*w+w+4;       //min 3.5 dla (-0.5,-0.5)
}

/*!
 * \brief Sympleks3D::maxSearcher
 * \return
 */
short Sympleks3D::maxSearcher()
{
    double temp_max;
    short index_max=0;
    int i=0;
    temp_max=y_3D[0];
    for(i=1;i<4;i++)
    {
        if(y_3D[i]>temp_max)
        {
            temp_max=y_3D[i];
            index_max=i;
        }
    }
    return index_max;
}

/*!
 * \brief Sympleks3D::minSearcher
 * \return
 */
short Sympleks3D::minSearcher()
{
    double temp_min;
    short index_min=0;
    int i=0;
    temp_min=y_3D[0];
    for(i=1;i<4;i++)
    {
        if(y_3D[i]<temp_min)
        {
            temp_min=y_3D[i];
            index_min=i;
        }
    }
    return index_min;
}
//do zmian
/*!
 * \brief Sympleks3D::Nelder_Meade
 */
void Sympleks3D::Nelder_Meade(void)
{
    short index_max=0;
    short index_min=0;
    int i=0;
    double centerSym[2]={0,0};
    double fcenterSym=0;
    double suma[2]={0,0};
    double* tempo=0;
    double* tempe=0;
    double* tempk=0;
    double fo=0,fe=0,fk=0;
    double eps=0.0000001;
    short licznik=0;        //na potrzeby warunku nr 8
    double min_pop=0;       //poprzednie minimum
    int iteracja=0;

    //Obliczenie wartosci funkcji w wierzcholkach
    for(i=0;i<4;i++)
        y_3D[i] = f(x1_3D[i],x2_3D[i]);

    while(fabs(y_3D[0]-y_3D[1])>eps || fabs(y_3D[1]-y_3D[2])>eps || fabs(y_3D[2]-y_3D[3])>eps)
    {
          ///////PUNKT ZWROTNY ALOGRYTMU///////////

        //Obliczenie wartosci funkcji w wierzcholkach
        for(i=0;i<4;i++)
            y_3D[i] = f(x1_3D[i],x2_3D[i]);

        for(i=0;i<4;i++)
            cout << x1_3D[i] <<","<< x2_3D[i] << endl;
        cout<<endl;

        min_pop=y_3D[index_min];

           //sprawdzanie czy z każdą iteracją funkcja celu maleje

        index_max=maxSearcher();
        index_min=minSearcher();


        if(iteracja>0 && min_pop<y_3D[index_min])
        {
            short i=0;
            srand( time(NULL) );
            for(i=0;i<4;i++)
            {
                x1_3D[i] = rand()%10 - 5;
                x2_3D[i] = rand()%10 - 5;

                //for(j=0;j<2;j++)
                //{
                //    x[i][j] = rand()%10 - 5;
                //}
            }
        }



        //Obliczenie puntku środka symetrii simpleksu
        for(i=0;i<4;i++)
        {
            suma[0]=suma[0]+x1_3D[i];
            suma[1]=suma[1]+x2_3D[i];
        }
        centerSym[0] = (suma[0]-x1_3D[index_max])/3;
        centerSym[1] = (suma[1]-x2_3D[index_max])/3;
        fcenterSym = f(centerSym[0],centerSym[1]);

        //SPRAWDZIĆ TEN SYF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        tempo = reflection(x1_3D[index_max],x2_3D[index_max],centerSym);
        fo=f(*tempo,*(tempo+1));
        if(fo<y_3D[index_min])
        {
            tempe = expansion(*tempo,*(tempo+1),centerSym);
            fe = f(*tempe,*(tempe+1));
            if(fe<y_3D[index_min])
            {
                x1_3D[index_max] = *tempe;
                x2_3D[index_max] = *(tempe+1);
            }
            else
            {
                x1_3D[index_max] = *tempo;
                x2_3D[index_max] = *(tempo+1);
            }
            //jezeli nie jest spelniony warunek stopu wroc do punktu zwrotnego
            if((fabs(y_3D[0]-y_3D[1])>eps || fabs(y_3D[1]-y_3D[2])>eps || fabs(y_3D[2]-y_3D[3])>eps))
                continue;
        }
        if(fo>y_3D[index_min])
        {
            for(i=0;i<index_max;i++)
            {
                if(fo>=y_3D[i])
                    licznik++;
            }
            for(i=index_max+1;i<4;i++)
            {
                if(fo>=y_3D[i])
                    licznik++;
            }

            if(licznik==3 && fo<y_3D[index_max])
            {
                x1_3D[index_max] = *tempo;
                x2_3D[index_max] = *(tempo+1);
            }
            tempk = contraction(x1_3D[index_max],x2_3D[index_max],centerSym);
            fk = f(*tempk,*(tempk+1));
            if(fk>=y_3D[index_max])
            {
                reduction(index_min);
            }
            else
            {
                x1_3D[index_max] = *tempk;
                x2_3D[index_max] = *(tempk+1);
                if((fabs(y_3D[0]-y_3D[1])>eps || fabs(y_3D[1]-y_3D[2])>eps || fabs(y_3D[2]-y_3D[3])>eps))
                    continue;
            }
            licznik=0;
            for(i=0;i<index_max;i++)
            {
                if(fo<y_3D[i])
                    licznik++;
            }
            for(i=index_max+1;i<4;i++)
            {
                if(fo<y_3D[i])
                    licznik++;
            }

            if(licznik==3)
            {
                x1_3D[index_max] = *tempo;
                x2_3D[index_max] = *(tempo+1);

            }
        }
        iteracja++;
    }
    cout<<"*********WYNIK**********"<<endl;
    cout<<"Wartosc minimalna funkcji to: "<<y_3D[index_min]<< " dla argumentu: "<<x1_3D[index_min]<<","<<x2_3D[index_min]<<endl;
    cout<<"Liczba iteracji: "<<iteracja<<endl;
}
