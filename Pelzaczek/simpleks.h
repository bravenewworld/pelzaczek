#ifndef SIMPLEKS_H
#define SIMPLEKS_H

#include <QCoreApplication>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <qcustomplot.h>
#include <QVector>

class Sympleks2D: public QObject
{
Q_OBJECT
public:
    float a,b,c;
    short pos[3];
    explicit Sympleks2D(QObject *parent = 0);
    ~Sympleks2D();
    //double x[3];
    QVector<double> x_2D;
    //double y[3];
    QVector<double> y_2D;
    double reflection(double p_h,double p);
    double expansion(double pp,double p);
    double contraction(double p_h,double p);
    void reduction(short index);
    double f(double q);
    short maxSearcher(void);
    short minSearcher(void);
    void Nelder_Meade(double eps);

/**
  Dla każdej akcji algorytmu zdefiniowano sygnał.
    Sygnał ten będzie przekazywał parametry położenia wierzchołków nowego sympleksu do widgetu rysującego.
  **/
signals:
    void did_reflection();
    void did_expansion();
    void did_contraction();
    void did_reduction();

public slots:
    void on_Stop();
    void on_Start();
};



class Sympleks3D: public QObject
{
    Q_OBJECT
private:

public:
    float a,b,c;
    short pos[4];
    explicit Sympleks3D(QObject *parent = 0);
    ~Sympleks3D();
    //double x[4][2];
    QVector<double> x1_3D,x2_3D;
    //double y[4];
    QVector<double> y_3D;
    double* reflection(double p_h1,double p_h2,double* p);
    double* expansion(double pp1,double pp2,double* p);
    //double* contraction(double* p_h,double* p);
    double* contraction(double p_h1,double p_h2,double* p);
    void reduction(short index);
    double f(double q, double w);
    short maxSearcher(void);
    short minSearcher(void);
    void Nelder_Meade(void);

signals:
    void did_reflection();
    void did_expansion();
    void did_contraction();
    void did_reduction();
};


#endif // SIMPLEKS_H

