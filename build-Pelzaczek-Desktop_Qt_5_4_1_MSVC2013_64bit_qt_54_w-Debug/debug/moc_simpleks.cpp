/****************************************************************************
** Meta object code from reading C++ file 'simpleks.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Pelzaczek/simpleks.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'simpleks.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Sympleks2D_t {
    QByteArrayData data[8];
    char stringdata[88];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Sympleks2D_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Sympleks2D_t qt_meta_stringdata_Sympleks2D = {
    {
QT_MOC_LITERAL(0, 0, 10), // "Sympleks2D"
QT_MOC_LITERAL(1, 11, 14), // "did_reflection"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 13), // "did_expansion"
QT_MOC_LITERAL(4, 41, 15), // "did_contraction"
QT_MOC_LITERAL(5, 57, 13), // "did_reduction"
QT_MOC_LITERAL(6, 71, 7), // "on_Stop"
QT_MOC_LITERAL(7, 79, 8) // "on_Start"

    },
    "Sympleks2D\0did_reflection\0\0did_expansion\0"
    "did_contraction\0did_reduction\0on_Stop\0"
    "on_Start"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Sympleks2D[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   48,    2, 0x0a /* Public */,
       7,    0,   49,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Sympleks2D::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Sympleks2D *_t = static_cast<Sympleks2D *>(_o);
        switch (_id) {
        case 0: _t->did_reflection(); break;
        case 1: _t->did_expansion(); break;
        case 2: _t->did_contraction(); break;
        case 3: _t->did_reduction(); break;
        case 4: _t->on_Stop(); break;
        case 5: _t->on_Start(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Sympleks2D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks2D::did_reflection)) {
                *result = 0;
            }
        }
        {
            typedef void (Sympleks2D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks2D::did_expansion)) {
                *result = 1;
            }
        }
        {
            typedef void (Sympleks2D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks2D::did_contraction)) {
                *result = 2;
            }
        }
        {
            typedef void (Sympleks2D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks2D::did_reduction)) {
                *result = 3;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Sympleks2D::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Sympleks2D.data,
      qt_meta_data_Sympleks2D,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Sympleks2D::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Sympleks2D::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Sympleks2D.stringdata))
        return static_cast<void*>(const_cast< Sympleks2D*>(this));
    return QObject::qt_metacast(_clname);
}

int Sympleks2D::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Sympleks2D::did_reflection()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Sympleks2D::did_expansion()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Sympleks2D::did_contraction()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void Sympleks2D::did_reduction()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
struct qt_meta_stringdata_Sympleks3D_t {
    QByteArrayData data[6];
    char stringdata[71];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Sympleks3D_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Sympleks3D_t qt_meta_stringdata_Sympleks3D = {
    {
QT_MOC_LITERAL(0, 0, 10), // "Sympleks3D"
QT_MOC_LITERAL(1, 11, 14), // "did_reflection"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 13), // "did_expansion"
QT_MOC_LITERAL(4, 41, 15), // "did_contraction"
QT_MOC_LITERAL(5, 57, 13) // "did_reduction"

    },
    "Sympleks3D\0did_reflection\0\0did_expansion\0"
    "did_contraction\0did_reduction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Sympleks3D[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,
       5,    0,   37,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Sympleks3D::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Sympleks3D *_t = static_cast<Sympleks3D *>(_o);
        switch (_id) {
        case 0: _t->did_reflection(); break;
        case 1: _t->did_expansion(); break;
        case 2: _t->did_contraction(); break;
        case 3: _t->did_reduction(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Sympleks3D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks3D::did_reflection)) {
                *result = 0;
            }
        }
        {
            typedef void (Sympleks3D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks3D::did_expansion)) {
                *result = 1;
            }
        }
        {
            typedef void (Sympleks3D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks3D::did_contraction)) {
                *result = 2;
            }
        }
        {
            typedef void (Sympleks3D::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Sympleks3D::did_reduction)) {
                *result = 3;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Sympleks3D::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Sympleks3D.data,
      qt_meta_data_Sympleks3D,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Sympleks3D::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Sympleks3D::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Sympleks3D.stringdata))
        return static_cast<void*>(const_cast< Sympleks3D*>(this));
    return QObject::qt_metacast(_clname);
}

int Sympleks3D::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void Sympleks3D::did_reflection()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Sympleks3D::did_expansion()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Sympleks3D::did_contraction()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void Sympleks3D::did_reduction()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
