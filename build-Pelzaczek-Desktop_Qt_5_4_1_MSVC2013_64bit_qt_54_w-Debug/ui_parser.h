/********************************************************************************
** Form generated from reading UI file 'parser.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARSER_H
#define UI_PARSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Parser
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *NewFunction;
    QLabel *label;
    QPushButton *Help;

    void setupUi(QDialog *Parser)
    {
        if (Parser->objectName().isEmpty())
            Parser->setObjectName(QStringLiteral("Parser"));
        Parser->resize(466, 225);
        buttonBox = new QDialogButtonBox(Parser);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(-140, 190, 601, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        NewFunction = new QLineEdit(Parser);
        NewFunction->setObjectName(QStringLiteral("NewFunction"));
        NewFunction->setGeometry(QRect(30, 120, 421, 41));
        label = new QLabel(Parser);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 25, 401, 81));
        Help = new QPushButton(Parser);
        Help->setObjectName(QStringLiteral("Help"));
        Help->setGeometry(QRect(30, 170, 61, 21));

        retranslateUi(Parser);
        QObject::connect(buttonBox, SIGNAL(accepted()), Parser, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Parser, SLOT(reject()));

        QMetaObject::connectSlotsByName(Parser);
    } // setupUi

    void retranslateUi(QDialog *Parser)
    {
        Parser->setWindowTitle(QApplication::translate("Parser", "Dialog", 0));
#ifndef QT_NO_TOOLTIP
        NewFunction->setToolTip(QApplication::translate("Parser", "<html><head/><body><p>Okno edycji funkcji - prosz\304\231 wpisa\304\207 wz\303\263r \305\274\304\205danej funkcji.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        NewFunction->setText(QString());
        label->setText(QApplication::translate("Parser", "<html><head/><body><p>Prosz\304\231 wpisa\304\207 \305\274\304\205dan\304\205 funkcj\304\231 w okno poni\305\274ej. </p><p>Program rozpoznaje typowe operatory i funkcje,<br/>lecz nie rozpoznaje formatowania tekstu (np. x1<span style=\" vertical-align:super;\">a </span>nale\305\274y zapisac jako x1^a).</p><p>Zmienne nale\305\274y oznaczy\304\207 jako y, x1,...,x5. Parser nie rozpozna innych zmiennych.</p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        Help->setToolTip(QApplication::translate("Parser", "<html><head/><body><p>Lista dost\304\231pnych operator\303\263w i funkcji.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        Help->setText(QApplication::translate("Parser", "Pomoc", 0));
    } // retranslateUi

};

namespace Ui {
    class Parser: public Ui_Parser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARSER_H
