/********************************************************************************
** Form generated from reading UI file 'interface.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_Interface
{
public:
    QAction *actionZaladuj_funkcje;
    QAction *actionZakoncz;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QCustomPlot *wykres;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *LoadFunction;
    QPushButton *Start;
    QPushButton *Pauza;
    QPushButton *Stop;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QSlider *StepSlider;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label_4;
    QSpinBox *IterationAmount;
    QLabel *label_5;
    QLineEdit *EditAlfa;
    QLabel *label;
    QLineEdit *EditBeta;
    QLabel *label_2;
    QLineEdit *EditGamma;
    QLabel *label_3;
    QLineEdit *EditEpsilon;
    QGroupBox *Zakres;
    QSpinBox *min_x1;
    QSpinBox *max_x1;
    QLabel *label_6;
    QLabel *label_7;
    QSpinBox *max_x2;
    QSpinBox *min_x2;
    QLabel *label_8;
    QSpinBox *max_x3;
    QSpinBox *min_x3;
    QLabel *label_9;
    QSpinBox *max_x4;
    QSpinBox *min_x4;
    QLabel *label_10;
    QSpinBox *max_x5;
    QSpinBox *min_x5;
    QMenuBar *menuBar;
    QMenu *menuPelzaczek;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Interface)
    {
        if (Interface->objectName().isEmpty())
            Interface->setObjectName(QStringLiteral("Interface"));
        Interface->resize(695, 603);
        actionZaladuj_funkcje = new QAction(Interface);
        actionZaladuj_funkcje->setObjectName(QStringLiteral("actionZaladuj_funkcje"));
        actionZakoncz = new QAction(Interface);
        actionZakoncz->setObjectName(QStringLiteral("actionZakoncz"));
        centralWidget = new QWidget(Interface);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(9, 9, 511, 481));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        wykres = new QCustomPlot(horizontalLayoutWidget);
        wykres->setObjectName(QStringLiteral("wykres"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(wykres->sizePolicy().hasHeightForWidth());
        wykres->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(wykres);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(550, 330, 121, 191));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        LoadFunction = new QPushButton(verticalLayoutWidget);
        LoadFunction->setObjectName(QStringLiteral("LoadFunction"));

        verticalLayout->addWidget(LoadFunction);

        Start = new QPushButton(verticalLayoutWidget);
        Start->setObjectName(QStringLiteral("Start"));

        verticalLayout->addWidget(Start);

        Pauza = new QPushButton(verticalLayoutWidget);
        Pauza->setObjectName(QStringLiteral("Pauza"));

        verticalLayout->addWidget(Pauza);

        Stop = new QPushButton(verticalLayoutWidget);
        Stop->setObjectName(QStringLiteral("Stop"));

        verticalLayout->addWidget(Stop);

        verticalLayoutWidget_3 = new QWidget(centralWidget);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(19, 510, 501, 31));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        StepSlider = new QSlider(verticalLayoutWidget_3);
        StepSlider->setObjectName(QStringLiteral("StepSlider"));
        StepSlider->setOrientation(Qt::Horizontal);

        verticalLayout_3->addWidget(StepSlider);

        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(540, 10, 135, 141));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_4);

        IterationAmount = new QSpinBox(layoutWidget);
        IterationAmount->setObjectName(QStringLiteral("IterationAmount"));

        formLayout->setWidget(1, QFormLayout::FieldRole, IterationAmount);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        EditAlfa = new QLineEdit(layoutWidget);
        EditAlfa->setObjectName(QStringLiteral("EditAlfa"));

        formLayout->setWidget(2, QFormLayout::FieldRole, EditAlfa);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        EditBeta = new QLineEdit(layoutWidget);
        EditBeta->setObjectName(QStringLiteral("EditBeta"));

        formLayout->setWidget(3, QFormLayout::FieldRole, EditBeta);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        EditGamma = new QLineEdit(layoutWidget);
        EditGamma->setObjectName(QStringLiteral("EditGamma"));

        formLayout->setWidget(4, QFormLayout::FieldRole, EditGamma);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_3);

        EditEpsilon = new QLineEdit(layoutWidget);
        EditEpsilon->setObjectName(QStringLiteral("EditEpsilon"));

        formLayout->setWidget(5, QFormLayout::FieldRole, EditEpsilon);

        Zakres = new QGroupBox(centralWidget);
        Zakres->setObjectName(QStringLiteral("Zakres"));
        Zakres->setGeometry(QRect(540, 160, 141, 171));
        min_x1 = new QSpinBox(Zakres);
        min_x1->setObjectName(QStringLiteral("min_x1"));
        min_x1->setEnabled(false);
        min_x1->setGeometry(QRect(40, 20, 42, 22));
        min_x1->setMinimum(-99);
        max_x1 = new QSpinBox(Zakres);
        max_x1->setObjectName(QStringLiteral("max_x1"));
        max_x1->setEnabled(false);
        max_x1->setGeometry(QRect(100, 20, 42, 22));
        label_6 = new QLabel(Zakres);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 20, 121, 16));
        label_7 = new QLabel(Zakres);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 50, 121, 16));
        max_x2 = new QSpinBox(Zakres);
        max_x2->setObjectName(QStringLiteral("max_x2"));
        max_x2->setEnabled(false);
        max_x2->setGeometry(QRect(100, 50, 42, 22));
        min_x2 = new QSpinBox(Zakres);
        min_x2->setObjectName(QStringLiteral("min_x2"));
        min_x2->setEnabled(false);
        min_x2->setGeometry(QRect(40, 50, 42, 22));
        min_x2->setMinimum(-99);
        label_8 = new QLabel(Zakres);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 80, 121, 16));
        max_x3 = new QSpinBox(Zakres);
        max_x3->setObjectName(QStringLiteral("max_x3"));
        max_x3->setEnabled(false);
        max_x3->setGeometry(QRect(100, 80, 42, 22));
        min_x3 = new QSpinBox(Zakres);
        min_x3->setObjectName(QStringLiteral("min_x3"));
        min_x3->setEnabled(false);
        min_x3->setGeometry(QRect(40, 80, 42, 22));
        min_x3->setMinimum(-99);
        label_9 = new QLabel(Zakres);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 110, 121, 16));
        max_x4 = new QSpinBox(Zakres);
        max_x4->setObjectName(QStringLiteral("max_x4"));
        max_x4->setEnabled(false);
        max_x4->setGeometry(QRect(100, 110, 42, 22));
        min_x4 = new QSpinBox(Zakres);
        min_x4->setObjectName(QStringLiteral("min_x4"));
        min_x4->setEnabled(false);
        min_x4->setGeometry(QRect(40, 110, 42, 22));
        min_x4->setMinimum(-99);
        label_10 = new QLabel(Zakres);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(10, 140, 121, 16));
        max_x5 = new QSpinBox(Zakres);
        max_x5->setObjectName(QStringLiteral("max_x5"));
        max_x5->setEnabled(false);
        max_x5->setGeometry(QRect(100, 140, 42, 22));
        min_x5 = new QSpinBox(Zakres);
        min_x5->setObjectName(QStringLiteral("min_x5"));
        min_x5->setEnabled(false);
        min_x5->setGeometry(QRect(40, 140, 42, 22));
        min_x5->setMinimum(-99);
        min_x1->raise();
        max_x1->raise();
        label_6->raise();
        label_9->raise();
        label_7->raise();
        max_x2->raise();
        min_x2->raise();
        label_8->raise();
        max_x3->raise();
        min_x3->raise();
        label_9->raise();
        max_x4->raise();
        min_x4->raise();
        label_10->raise();
        max_x5->raise();
        min_x5->raise();
        Interface->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Interface);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 695, 23));
        menuPelzaczek = new QMenu(menuBar);
        menuPelzaczek->setObjectName(QStringLiteral("menuPelzaczek"));
        Interface->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Interface);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Interface->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Interface);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Interface->setStatusBar(statusBar);

        menuBar->addAction(menuPelzaczek->menuAction());
        menuPelzaczek->addAction(actionZaladuj_funkcje);
        menuPelzaczek->addAction(actionZakoncz);

        retranslateUi(Interface);

        QMetaObject::connectSlotsByName(Interface);
    } // setupUi

    void retranslateUi(QMainWindow *Interface)
    {
        Interface->setWindowTitle(QApplication::translate("Interface", "Interface", 0));
        actionZaladuj_funkcje->setText(QApplication::translate("Interface", "Za\305\202aduj funkcj\304\231", 0));
        actionZakoncz->setText(QApplication::translate("Interface", "Zako\305\204cz", 0));
        LoadFunction->setText(QApplication::translate("Interface", "Za\305\202aduj funkcj\304\231", 0));
        Start->setText(QApplication::translate("Interface", "Start", 0));
        Pauza->setText(QApplication::translate("Interface", "Pauza", 0));
        Stop->setText(QApplication::translate("Interface", "Stop", 0));
        label_4->setText(QApplication::translate("Interface", " Liczba iteracji", 0));
#ifndef QT_NO_TOOLTIP
        IterationAmount->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wybierz liczb\304\231 iteracji algorytmu.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("Interface", " wsp. alfa", 0));
#ifndef QT_NO_TOOLTIP
        EditAlfa->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\261</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("Interface", " wsp. beta", 0));
#ifndef QT_NO_TOOLTIP
        EditBeta->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\262.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("Interface", " wsp. gamma", 0));
#ifndef QT_NO_TOOLTIP
        EditGamma->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \311\243.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("Interface", " wsp. epsilon", 0));
#ifndef QT_NO_TOOLTIP
        EditEpsilon->setToolTip(QApplication::translate("Interface", "<html><head/><body><p>Wprowad\305\272 wsp\303\263\305\202czynnik \316\265.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        Zakres->setTitle(QApplication::translate("Interface", "Zakres zmiennych", 0));
        label_6->setText(QApplication::translate("Interface", "x1 od                do", 0));
        label_7->setText(QApplication::translate("Interface", "x2 od                do", 0));
        label_8->setText(QApplication::translate("Interface", "x3 od                do", 0));
        label_9->setText(QApplication::translate("Interface", "x4 od                do", 0));
        label_10->setText(QApplication::translate("Interface", "x5 od                do", 0));
        menuPelzaczek->setTitle(QApplication::translate("Interface", "Pe\305\202zaczek", 0));
    } // retranslateUi

};

namespace Ui {
    class Interface: public Ui_Interface {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTERFACE_H
